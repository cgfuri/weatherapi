package za.co.cgf.myweatherapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import za.co.cgf.myweatherapp.control.HttpHandler;

public class MainActivity extends AppCompatActivity
{
    private FirebaseAuth mAuth;
    HashMap<String, String> contactList;
    private String TAG = "tag_Errors";//MainActivity.class.getSimpleName();
    DatabaseReference myRef;
    private LinearLayout linLayMainActivity,linLayMainDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        if(mAuth.getCurrentUser() == null)
        {
            this.finish();
        }

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users").child(mAuth.getUid()).child("Search_History");

        contactList = new HashMap<>();

        new GetContacts().execute();
    }

    @Override
    public void onBackPressed()
    {
        super.onBackPressed();
        this.finish();
        mAuth.signOut();
    }

    private void updateUI(HashMap<String, String> contactList)
    {
        //Display info on UI
        // get values from HashMap
        String description = contactList.get("description");

        String temp =contactList.get("temp");
        String temp_max = contactList.get("temp_max");
        String temp_min =contactList.get("temp_min");
        String humidity =contactList.get("humidity");

        String speed =contactList.get("speed");
        String deg =contactList.get("deg");

        String name = contactList.get("name");
        String direction = contactList.get("direction");

        //reference UI elements
        linLayMainActivity = findViewById(R.id.linLayMainActivity);
        linLayMainDisplay = findViewById(R.id.linLayMainDisplay);

        TextView txtDescription = findViewById(R.id.txtDescription);
        TextView txtTemp = findViewById(R.id.txtTemp);
        TextView txtMin = findViewById(R.id.txtMin);
        TextView txtMax = findViewById(R.id.txtMax);
        TextView txtHumid = findViewById(R.id.txtHumid);
        TextView txtSpeed = findViewById(R.id.txtSpeed);
        TextView txtDegrees = findViewById(R.id.txtDegrees);
        TextView txtName = findViewById(R.id.txtName);

        //add Hashmap values to textviews
        Resources res = getResources();

        txtDescription.setText(String.format(res.getString(R.string.description), description));
        txtTemp.setText(String.format(res.getString(R.string.temperature), temp));
        txtMin.setText(String.format(res.getString(R.string.min_temp), temp_min));
        txtMax.setText(String.format(res.getString(R.string.max_temp), temp_max));
        txtHumid.setText(String.format(res.getString(R.string.humidity), humidity));
        txtSpeed.setText(String.format(res.getString(R.string.wind_speed), speed));
        txtDegrees.setText(String.format(res.getString(R.string.wind_direction), direction,deg));

        txtName.setText(name);

        linLayMainDisplay.setVisibility(View.VISIBLE);

        linLayMainActivity.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                new GetContacts().execute();
            }
        });
    }

    public Result getLocation()
    {
        try
        {   //Use lat and lon for api call
            Location location = null;
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                String locationProvider = LocationManager.NETWORK_PROVIDER;
                LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                //lm.requestSingleUpdate(locationProvider, null);//TODO
                location = lm.getLastKnownLocation(locationProvider);
            }
            return new Result(location.getLatitude(), location.getLongitude());
        }
        catch(NullPointerException e)
        {
            Log.e(TAG, "Couldn't get access to Location : " + e.getMessage());
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    Toast.makeText(getApplicationContext(), "Couldn't get access to Location - Check Permissions", Toast.LENGTH_LONG).show();
                }
            });
        }
        return new Result(9999, 9999);
    }

    public String getDirection(double degrees)
    {
        String direction = "";
        if(degrees>=337.5 && degrees <= 22.5)
        {
            direction = "North";
        }
        else if (degrees>=22.6 && degrees <= 67.5)
        {
            direction = "North East";
        }
        else if (degrees>=67.6 && degrees <= 112.5)
        {
            direction = "East";
        }
        else if (degrees>=112.6 && degrees <= 157.5)
        {
            direction = "South East";
        }
        else if (degrees>=157.6 && degrees <= 202.5)
        {
            direction = "South";
        }
        else if (degrees>=202.6 && degrees <= 247.5)
        {
            direction = "South West";
        }
        else if (degrees>=247.6 && degrees <= 245.5)
        {
            direction = "West";
        }
        else if (degrees>=245.6 && degrees <= 337.4)
        {
            direction = "North West";
        }


            return direction;
    }

    private class GetContacts extends AsyncTask<Void, Void, Void>
    {
       // String message;
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
            Toast.makeText(MainActivity.this,"Obtaining data",Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(Void... arg0)
        {

            Result rs =getLocation();
            if(!rs.getStatus())
            {
                Toast.makeText(getApplicationContext(), "Couldn't Retrieve Location ", Toast.LENGTH_LONG).show();
                return null;
            }

            HttpHandler sh = new HttpHandler();
            String url = "http://api.openweathermap.org/data/2.5/weather?lat="+rs.getLatitude()+"&lon="+rs.getLongitude()+"&APPID=83b5ba99848dd6b3ddae34c2200588a7&units=metric";
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);
            if (jsonStr != null)
            {
                try
                {
                    JSONObject jsonObj = new JSONObject(jsonStr);//Main

                    //Get weather and children
                    String main_temp = jsonObj.getJSONArray("weather").getJSONObject(0).optString("main");
                    String description = jsonObj.getJSONArray("weather").getJSONObject(0).optString("description");//TODO
                    Log.e(TAG, "Response from url: " + description + " " + main_temp);

                    //Get main and children
                    JSONObject main =jsonObj.getJSONObject("main");//group
                    double temperature =main.optDouble("temp");//temp
                    double min =main.optDouble("temp_min");//tempmin
                    double max =main.optDouble("temp_max");//tempmax
                    double humidity =main.optDouble("humidity");//tempmax
                    //Get wind and children
                    JSONObject wind =jsonObj.getJSONObject("wind");//group
                    double speed =wind.optDouble("speed");//temp
                    double deg =wind.optDouble("deg");//tempmin

                    String direction =getDirection(wind.optDouble("deg"));
                    //Get clouds and children
                    JSONObject cloud =jsonObj.getJSONObject("clouds");//group
                    double clouds =cloud.optDouble("clouds");//temp
                    //get location name
                    String locName = jsonObj.get("name").toString();

                        //Store all recieved values in Hasmap
                        HashMap<String, String> contact = new HashMap<>();

                        contact.put("main", main_temp);
                        contact.put("description", description);

                        contact.put("temp", Double.toString(temperature));
                        contact.put("temp_min", Double.toString(min));
                        contact.put("temp_max", Double.toString(max));
                        contact.put("humidity", Double.toString(humidity));

                        contact.put("speed", Double.toString(speed));
                        contact.put("deg", Double.toString(deg));
                        contact.put("direction", direction);

                        contact.put("clouds", Double.toString(clouds));

                        contact.put("name", locName);

                        contactList=contact;

                    Calendar c = Calendar.getInstance();

                    //write to firebase
                    DatabaseReference ref = myRef.child(c.getTime().toString());

                    DatabaseReference weatherRef =ref.child("weather");
                    weatherRef.child("main").setValue(main_temp);
                    weatherRef.child("description").setValue(description);

                    DatabaseReference mainRef =ref.child("main");
                    mainRef.child("temp").setValue(Double.toString(temperature));
                    mainRef.child("temp_min").setValue(Double.toString(min));
                    mainRef.child("temp_max").setValue(Double.toString(max));
                    mainRef.child("humidity").setValue(Double.toString(humidity));

                    DatabaseReference windRef =ref.child("wind");
                    windRef.child("speed").setValue(Double.toString(speed));
                    windRef.child("deg").setValue(Double.toString(deg));

                    DatabaseReference cloudRef =ref.child("clouds");
                    cloudRef.child("clouds").setValue(Double.toString(clouds));

                    DatabaseReference nameRef =ref.child("name");
                    nameRef.setValue(locName);

                }
                catch (final JSONException e)
                {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Toast.makeText(getApplicationContext(), "Error displaying info", Toast.LENGTH_LONG).show();
                        }
                    });
                }

            }
            else
            {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Toast.makeText(getApplicationContext(), "Couldn't get data from server.", Toast.LENGTH_LONG).show();
                    }
                });
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            super.onPostExecute(result);
            //dispaly values in UI
            updateUI(contactList);
        }
    }

}
