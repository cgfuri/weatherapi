package za.co.cgf.myweatherapp;

final class Result
{
    private final double latitude;
    private final double longitude;
    private boolean status = true;

    public Result(double latitude, double longitude)
    {
        this.latitude = latitude;
        this.longitude = longitude;

        if (latitude == 9999)
        {
            status = false;
        }
    }

    public String getLatitude()
    {
        return Double.toString(latitude);
    }

    public String getLongitude()
    {
        return Double.toString(longitude);
    }

    public boolean getStatus()
    {
        return status;
    }
}
